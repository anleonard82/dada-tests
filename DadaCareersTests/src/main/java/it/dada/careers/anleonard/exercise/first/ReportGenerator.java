package it.dada.careers.anleonard.exercise.first;

import java.io.File;
import java.io.IOException;
import java.util.List;

import it.dada.careers.anleonard.exercise.first.pojo.DailyReportRow;
import it.dada.careers.anleonard.exercise.first.pojo.LogRow;

/**
 * Generate report
 *
 * @author Andrea Nardi
 *
 */
public class ReportGenerator {

	/**
	 * Generate the report from the input csv file
	 *
	 * @param inputFile
	 * @param reportFile
	 * @throws IOException
	 */
	public static void generate(File inputFile, File reportFile) throws IOException {

		//read the input file and obtain the rows list
		List<LogRow> rowList = ReportFileManager.read(inputFile);

		List<DailyReportRow> rows = ReportRowManager.convert2csv(rowList);

		//write the rows list in the report file
		ReportFileManager.write(rows, reportFile);
	}

}
