package it.dada.careers.anleonard.exercise.second;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * 
 * @author Andrea Nardi
 *
 */
public class CalculatorArrayFormatter {
	
	/**
	 * The method convert the number to an array of digits
	 * 
	 * @param number
	 * @return
	 */
	static Integer[] convert(Integer number) {

		//store in an array the single digits of the number
		List<Integer> numberDigits = new ArrayList<Integer>();

		Integer[] digits = getDigitsByInteger(number);

		for (Integer current : digits) {
			numberDigits.add(current);
		}
		Collections.reverse(numberDigits);
		return numberDigits.toArray(new Integer[numberDigits.size()]);
	}

	private static Integer[] getDigitsByInteger(Integer num)
	{
	    int digitCount = Integer.toString(num).length();

	    if (num < 0)
	        digitCount--;

	    Integer[] result = new Integer[digitCount];

	    while (digitCount-- >0) {
	        result[digitCount] = num % 10;
	        num /= 10;
	    }
	    return result;
	}

}
