package it.dada.careers.anleonard.exercise.second.operation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class OperationManager {

	public Integer[] firstNumber;
	public Integer[] secondNumber;

	public OperationType operationType;

	public static Integer[] calc(Integer[] firstNumber, Integer[] secondNumber, int operationType) {

		Integer[] result = null;

		switch (operationType) {

			case OperationType.SUM: result = sum(firstNumber, secondNumber); break;
			case OperationType.MULTIPLICATION: result = multiply(firstNumber, secondNumber); break;
			//Add here new operations mapping

			default: break;
		}

		return result;
	}

	/**
	 * Private method for sum two Integer stored in array
	 *
	 * @param firstNumber
	 * @param secondNumber
	 * @return
	 */
	private static Integer[] sum(Integer[] firstNumber, Integer[] secondNumber){

		List<Integer> result = new ArrayList<Integer>();

		Integer[] shorterNumber;
		Integer[] longerNumber;

		//dimensions difference
		int sizeDifference = 0;

		//find the shorter and the longer number
		if (firstNumber.length > secondNumber.length) {

			longerNumber = firstNumber;
			sizeDifference = firstNumber.length-secondNumber.length;
			shorterNumber = Arrays.copyOf(secondNumber, firstNumber.length);

		} else {

			longerNumber = secondNumber;
			sizeDifference = secondNumber.length-firstNumber.length;
			shorterNumber = Arrays.copyOf(firstNumber, secondNumber.length);

		}

		Arrays.fill(shorterNumber, shorterNumber.length - sizeDifference, longerNumber.length, 0);

		int discard = 0;
		int decimalPart = 0;

		for (int i = 0; i < shorterNumber.length; i++) {

			//sum the digit in the same position
			int sumDigit = shorterNumber[i] + longerNumber[i];

			decimalPart = (sumDigit + discard) % 10;

			result.add(decimalPart);

			discard = (sumDigit + discard) / 10;
		}

		if (discard > 0) {

			result.add(discard);

		}

		return result.toArray(new Integer[result.size()]);
	}

	/**
	 * Private method for multiply two Integer stored in array
	 *
	 * @param firstNumber
	 * @param secondNumber
	 * @return
	 */
	private static Integer[] multiply(Integer[] firstNumber, Integer[] secondNumber){

		Integer[] totalSum = new Integer[]{};

		for (int i = 0; i < firstNumber.length; i++) {

			Integer[] sum = new Integer[]{};

			for (Integer j = 0; j < firstNumber[i]; j++) {
				sum = OperationManager.calc(sum, secondNumber, OperationType.SUM);
			}

			if (i > 0) {

				Integer[] newArray = new Integer[sum.length + i];

				System.arraycopy(sum, 0, newArray, i, sum.length);

				Arrays.fill(newArray, 0, i, 0);

				totalSum =  OperationManager.calc(newArray, totalSum, OperationType.SUM);

			} else {
				totalSum = sum;
			}

		}

		return totalSum;
	}
}
