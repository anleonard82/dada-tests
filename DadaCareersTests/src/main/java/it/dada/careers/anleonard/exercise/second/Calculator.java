package it.dada.careers.anleonard.exercise.second;

import java.util.ArrayList;
import java.util.List;

import it.dada.careers.anleonard.exercise.second.operation.OperationManager;
import it.dada.careers.anleonard.exercise.second.operation.OperationType;

/**
 * 
 * @author Andrea Nardi
 *
 */
public class Calculator {

	/**
	 * The method to multiply integer numbers using the addition operator (+)	instead the multiplication operator (x).
	 * The result will be stored the values in arrays.
	 * 
	 * @param numbers
	 * @return the operation result
	 */
	public static Integer[] calculate(Integer[] numbers) {
		
		List<Integer[]> numbersInArrays = new ArrayList<Integer[]>();
		
		for (int number : numbers) {
			numbersInArrays.add(CalculatorArrayFormatter.convert(number));
		}

		Integer[] product = new Integer[]{1};

		for (Integer[] multiplicandArray : numbersInArrays) {
			
			//multiplicate the current value with the previous value
			product = OperationManager.calc(product, multiplicandArray, OperationType.MULTIPLICATION);
		}

		return product;
	}
}
