package it.dada.careers.anleonard.exercise.second.operation;

/**
 * Operation type constants
 *
 * @author Andrea Nardi
 *
 */
public class OperationType {

    public static final int SUM = 0;
    public static final int MULTIPLICATION = 1;
    //Add here new operation type

}