package it.dada.careers.anleonard.exercise.first;

public class CsvConstans {

    public static final int HTTP_STATUS_SUCCESS = 200;

	public static final int CSV_TIMESTAMP_ROW_INDEX = 0;
	public static final int CSV_BYTES_ROW_INDEX = 1;
	public static final int CSV_HTTPSTATUS_ROW_INDEX = 2;
	public static final int CSV_REMOTEADDRESSIP_ROW_INDEX = 3;

	public static final Character EXCEL_DELIMITER = ';';

}
