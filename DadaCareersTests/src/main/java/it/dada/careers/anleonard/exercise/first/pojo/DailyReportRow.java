package it.dada.careers.anleonard.exercise.first.pojo;

public class DailyReportRow {

	private String ipAddress;
	private Integer numberOfRequests;
	private float percentageOfTotalAmountRequests;
	private Integer totalBytesSent;
	private float percentageOfTotalAmountBytes;

	public String getIpAddress() {
		return ipAddress;
	}
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	public Integer getNumberOfRequests() {
		return numberOfRequests;
	}
	public void setNumberOfRequests(Integer numberOfRequests) {
		this.numberOfRequests = numberOfRequests;
	}
	public float getPercentageOfTotalAmountRequests() {
		return percentageOfTotalAmountRequests;
	}
	public void setPercentageOfTotalAmountRequests(float percentageOfTotalAmountRequests) {
		this.percentageOfTotalAmountRequests = percentageOfTotalAmountRequests;
	}
	public Integer getTotalBytesSent() {
		return totalBytesSent;
	}
	public void setTotalBytesSent(Integer totalBytesSent) {
		this.totalBytesSent = totalBytesSent;
	}
	public float getPercentageOfTotalAmountBytes() {
		return percentageOfTotalAmountBytes;
	}
	public void setPercentageOfTotalAmountBytes(float percentageOfTotalAmountBytes) {
		this.percentageOfTotalAmountBytes = percentageOfTotalAmountBytes;
	}
	@Override
	public String toString() {
		return new StringBuilder(ipAddress).append(",").append(numberOfRequests).append(",").append(percentageOfTotalAmountRequests)
				.append(",").append(totalBytesSent).append(",").append(percentageOfTotalAmountBytes).toString();
	}

}
