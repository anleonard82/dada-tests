package it.dada.careers.anleonard.exercise.first.pojo;

public class LogRow {

	private String timestamp;
	private Integer bytes;
	private Integer httpStatus;
	private String remoteAddressIp;

	public String getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}
	public Integer getBytes() {
		return bytes;
	}
	public void setBytes(Integer bytes) {
		this.bytes = bytes;
	}
	public Integer getHttpStatus() {
		return httpStatus;
	}
	public void setHttpStatus(Integer httpStatus) {
		this.httpStatus = httpStatus;
	}
	public String getRemoteAddressIp() {
		return remoteAddressIp;
	}
	public void setRemoteAddressIp(String remoteAddressIp) {
		this.remoteAddressIp = remoteAddressIp;
	}
}
