package it.dada.careers.anleonard.exercise.first;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import it.dada.careers.anleonard.exercise.first.pojo.DailyReportRow;
import it.dada.careers.anleonard.exercise.first.pojo.LogRow;

/**
 * Report rows manager
 *
 * @author Andrea Nardi
 *
 */
public class ReportRowManager {

	static Map<String, DailyReportRow> ipReportMap = new HashMap<String, DailyReportRow>();
	static List<DailyReportRow> result = new ArrayList<DailyReportRow>();

	static float totalRequestsSent = 0;
	static float totalBytesSent = 0;

	private static void populateRows(){

		for (String ip : ipReportMap.keySet()) {

			DailyReportRow rRow = ipReportMap.get(ip);

			rRow.setPercentageOfTotalAmountBytes(rRow.getTotalBytesSent() / totalBytesSent * 100);

			rRow.setPercentageOfTotalAmountRequests(rRow.getNumberOfRequests() / totalRequestsSent * 100);

			ipReportMap.put(ip, rRow);

			result.add(rRow);
		}
	}

	/**
	 * Sort by number or request
	 * @param rowsList
	 */
	private static void sort(){
		Collections.sort(result, new Comparator<DailyReportRow>() {
			public int compare(DailyReportRow o1, DailyReportRow o2) {
				return o1.getNumberOfRequests() > o2.getNumberOfRequests() ? -1 : 1;
			}
		});
	}

	/**
	 * Convert log rows to csv file
	 *
	 * @param logRows
	 * @return
	 */
	public static List<DailyReportRow> convert2csv(List<LogRow> logRows) {

		//foreach logRow
		for (LogRow logRow : logRows) {

			// exclude from the report the lines where the STATUS is different from HTTP_STATUS_SUCCESS
			if (!logRow.getHttpStatus().equals(CsvConstans.HTTP_STATUS_SUCCESS)) {
				continue;
			}

			//if the map contains the remote address ip
			if (ipReportMap.containsKey(logRow.getRemoteAddressIp())) {

				//get the report row
				DailyReportRow rRow = ipReportMap.get(logRow.getRemoteAddressIp());

				//increment the number of requests
				rRow.setNumberOfRequests(rRow.getNumberOfRequests() + 1);

				//increment the number of bytes sent
				rRow.setTotalBytesSent(rRow.getTotalBytesSent() + logRow.getBytes());

				//update the report row
				ipReportMap.put(logRow.getRemoteAddressIp(), rRow);

			} else {

				//create a new DailyReportRow
				DailyReportRow rRow = new DailyReportRow();

				//set the ip address
				rRow.setIpAddress(logRow.getRemoteAddressIp());

				//set the number of request to one
				rRow.setNumberOfRequests(1);

				//set the number of bytes
				rRow.setTotalBytesSent(logRow.getBytes());

				//put the report row
				ipReportMap.put(logRow.getRemoteAddressIp(), rRow);

			}

			totalRequestsSent++;

			totalBytesSent += logRow.getBytes();
		}

		populateRows();

		sort();

		ipReportMap = new HashMap<String, DailyReportRow>();

		totalRequestsSent = 0;
		totalBytesSent = 0;

		return result;
	}

}
