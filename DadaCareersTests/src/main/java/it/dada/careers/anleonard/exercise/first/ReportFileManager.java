package it.dada.careers.anleonard.exercise.first;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.io.FileUtils;
import it.dada.careers.anleonard.exercise.first.pojo.DailyReportRow;
import it.dada.careers.anleonard.exercise.first.pojo.LogRow;

/**
 * Report file manger
 *
 * @author Andrea Nardi
 *
 */
public class ReportFileManager {

	/**
	 * Write report rows to report file
	 *
	 * @param reportFile
	 * @param reportRows
	 * @throws IOException
	 */
	public static void write(List<DailyReportRow> reportRows, File reportFile) throws IOException{

		if(reportRows.size()>0){
			for (DailyReportRow reportRow : reportRows) {
				FileUtils.writeStringToFile(reportFile, reportRow.toString() + System.lineSeparator(), Charset.defaultCharset(), true);
			}
		}
	}

	/**
	 * Read input report file
	 *
	 * @param inputFile
	 * @return
	 * @throws IOException
	 */
	public static List<LogRow> read(File inputFile) throws IOException {

		List<LogRow> result = new ArrayList<LogRow>();

		CSVParser parser = CSVParser.parse(inputFile, Charset.defaultCharset(), CSVFormat.EXCEL.withDelimiter(CsvConstans.EXCEL_DELIMITER));

		for (CSVRecord csvRecord : parser) {

			LogRow lRow = new LogRow();

			lRow.setTimestamp(csvRecord.get(CsvConstans.CSV_TIMESTAMP_ROW_INDEX));
			lRow.setBytes(Integer.valueOf(csvRecord.get(CsvConstans.CSV_BYTES_ROW_INDEX)));
			lRow.setHttpStatus(Integer.valueOf(csvRecord.get(CsvConstans.CSV_HTTPSTATUS_ROW_INDEX)));
			lRow.setRemoteAddressIp(csvRecord.get(CsvConstans.CSV_REMOTEADDRESSIP_ROW_INDEX));

			result.add(lRow);
		}
		return result;
	}

}
