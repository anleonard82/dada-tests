package it.dada.careers.anleonard.exercise.first;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.junit.Before;
import org.junit.Test;

import it.dada.careers.anleonard.exercise.first.ReportRowManager;
import it.dada.careers.anleonard.exercise.first.ReportGenerator;
import it.dada.careers.anleonard.exercise.first.pojo.DailyReportRow;
import it.dada.careers.anleonard.exercise.first.pojo.LogRow;

public class ReportTester {

	final String REQUESTS_LOG_PATH = "logs/requests.log";
	final String REPORT_PATH = "results/ipaddr.csv";

	private File sourceLogFile;

	@Before
	public void init() {
		sourceLogFile = new File(getClass().getClassLoader().getResource(REQUESTS_LOG_PATH).getFile());
	}

	@Test
	public void testRead() throws IOException {
		List<LogRow> logRows = ReportFileManager.read(sourceLogFile);
		assertNotNull(logRows);
		assertEquals(logRows.size(), 35);
		assertTrue(logRows.get(2).getBytes() == 217088);
	}

	@Test
	public void testConvert2Csv() throws IOException {

		List<DailyReportRow> reportRows = ReportRowManager.convert2csv(ReportFileManager.read(sourceLogFile));

		DailyReportRow reportRow = reportRows.get(0);

		assertEquals("210.113.188.123", reportRow.getIpAddress());
		assertTrue(reportRow.getTotalBytesSent() == 1108124);
	}

	@Test
	public void testGenerate() throws IOException {

		File reportFile = new File(getClass().getClassLoader().getResource(REPORT_PATH).getFile());

		FileUtils.writeStringToFile(reportFile, null, Charset.defaultCharset());

		assertEquals(0, FileUtils.sizeOf(reportFile));

		ReportGenerator.generate(sourceLogFile, reportFile);
	}
}
