package it.dada.careers.anleonard.exercise.second;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;

/**
 * Calculator tester
 *
 * Created by Andrea Nardi
 */
public class CalculatorTester {

	@Test
	public void testCalculate() {

		assertArrayEquals(Calculator.calculate(CalculatorTestCases.tc1), CalculatorTestCases.tc1result);
		assertArrayEquals(Calculator.calculate(CalculatorTestCases.tc2), CalculatorTestCases.tc2result);
		assertArrayEquals(Calculator.calculate(CalculatorTestCases.tc3), CalculatorTestCases.tc3result);
	}

	@Test
	public void testConvert() {
		Integer[] result = CalculatorArrayFormatter.convert(58);
		assertNotNull(result);
		assertArrayEquals(result, new Integer[]{8, 5});
	}

}
