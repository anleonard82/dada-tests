package it.dada.careers.anleonard.exercise.second;

public class OperationTestCases {

	public static final Integer[] tc1a = new Integer[]{7, 8, 5, 2};
	public static final Integer[] tc1b = new Integer[]{6, 6};
	public static final Integer[] tc1sum = new Integer[]{3, 5, 6, 2};
	public static final Integer[] tc1mul = new Integer[]{2, 4, 7, 0, 7, 1};

	public static final Integer[] tc2a = new Integer[]{1, 3, 6};
	public static final Integer[] tc2b = new Integer[]{1, 7};
	public static final Integer[] tc2sum = new Integer[]{2, 0, 7};
	public static final Integer[] tc2mul = new Integer[]{1, 0, 8, 4, 4};

	public static final Integer[] tc3a = new Integer[]{6};
	public static final Integer[] tc3b = new Integer[]{7};
	public static final Integer[] tc3mul = new Integer[]{2, 4};
	public static final Integer[] tc3sum = new Integer[]{3, 1};

	public static final Integer[] tc4a = new Integer[]{5, 7};
	public static final Integer[] tc4b = new Integer[]{9};
	public static final Integer[] tc4mul = new Integer[]{5, 7, 6};
	public static final Integer[] tc4sum = new Integer[]{4, 8};

	public static final Integer[] tc5a = new Integer[]{6, 6, 1};
	public static final Integer[] tc5b = new Integer[]{0, 1, 2};
	public static final Integer[] tc5mul = new Integer[]{0, 6, 8, 4, 3};
	public static final Integer[] tc5sum = new Integer[]{6, 7, 3};

	public static final Integer[] tc6a = new Integer[]{5, 4, 2};
	public static final Integer[] tc6b = new Integer[]{8, 5, 0, 1};
	public static final Integer[] tc6mul = new Integer[]{0, 1, 2, 9, 5, 2};
	public static final Integer[] tc6sum = new Integer[]{3, 0, 3, 1};

}