package it.dada.careers.anleonard.exercise.second;

public class CalculatorTestCases {

	public static final Integer[] tc1 = new Integer[]{22,17};
	public static final Integer[] tc1result = new Integer[]{4, 7, 3};

	public static final Integer[] tc2 = new Integer[]{87,211};
	public static final Integer[] tc2result = new Integer[]{7, 5, 3, 8, 1};

	public static final Integer[] tc3 = new Integer[]{641, 12, 33};
	public static final Integer[] tc3result = new Integer[]{6, 3, 8, 3, 5, 2};

}