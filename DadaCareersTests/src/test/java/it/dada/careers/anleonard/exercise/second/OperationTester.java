package it.dada.careers.anleonard.exercise.second;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;

import it.dada.careers.anleonard.exercise.second.operation.OperationManager;
import it.dada.careers.anleonard.exercise.second.operation.OperationType;

/**
 * Operations tester
 *
 * @author Andrea Nardi
 *
 */
public class OperationTester {

	@Test
	public void testSum() {

		Integer[] result = OperationManager.calc(OperationTestCases.tc1a, OperationTestCases.tc1b, OperationType.SUM);
		assertNotNull(result);
		assertArrayEquals(result, OperationTestCases.tc1sum);

		result = OperationManager.calc(OperationTestCases.tc2a, OperationTestCases.tc2b, OperationType.SUM);
		assertNotNull(result);
		assertArrayEquals(result, OperationTestCases.tc2sum);

		result = OperationManager.calc(OperationTestCases.tc3a, OperationTestCases.tc3b, OperationType.SUM);
		assertNotNull(result);
		assertArrayEquals(result, OperationTestCases.tc3sum);

		result = OperationManager.calc(OperationTestCases.tc4a, OperationTestCases.tc4b, OperationType.SUM);
		assertNotNull(result);
		assertArrayEquals(result, OperationTestCases.tc4sum);

		result = OperationManager.calc(OperationTestCases.tc5a, OperationTestCases.tc5b, OperationType.SUM);
		assertNotNull(result);
		assertArrayEquals(result, OperationTestCases.tc5sum);

		result = OperationManager.calc(OperationTestCases.tc6a, OperationTestCases.tc6b, OperationType.SUM);
		assertNotNull(result);
		assertArrayEquals(result, OperationTestCases.tc6sum);

	}

	@Test
	public void testMultiply() {

		Integer[] testResult = OperationManager.calc(OperationTestCases.tc1a, OperationTestCases.tc1b, OperationType.MULTIPLICATION);
		assertNotNull(testResult);
		assertArrayEquals(testResult, OperationTestCases.tc1mul);

		testResult = OperationManager.calc(OperationTestCases.tc2a, OperationTestCases.tc2b, OperationType.MULTIPLICATION);
		assertNotNull(testResult);
		assertArrayEquals(testResult, OperationTestCases.tc2mul);

		testResult = OperationManager.calc(OperationTestCases.tc3a, OperationTestCases.tc3b, OperationType.MULTIPLICATION);
		assertNotNull(testResult);
		assertArrayEquals(testResult, OperationTestCases.tc3mul);

		//Test Case 2
		testResult = OperationManager.calc(OperationTestCases.tc4a, OperationTestCases.tc4b, OperationType.MULTIPLICATION);
		assertNotNull(testResult);
		assertArrayEquals(testResult, OperationTestCases.tc4mul);

		//Test Case 3
		testResult = OperationManager.calc(OperationTestCases.tc5a, OperationTestCases.tc5b, OperationType.MULTIPLICATION);
		assertNotNull(testResult);
		assertArrayEquals(testResult, OperationTestCases.tc5mul);

		//Test Case 4
		testResult = OperationManager.calc(OperationTestCases.tc6a, OperationTestCases.tc6b, OperationType.MULTIPLICATION);
		assertNotNull(testResult);
		assertArrayEquals(testResult, OperationTestCases.tc6mul);
	}

	//Add here new operation tests

}
